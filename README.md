# Framework for configurable video analysis
----
Created by: Benedek Molnár | molnaben | molnaben@fit.cvut.cz
<br>
Supervisor: Ing. Jan Hejda, Ph.D. | jan.hejda@cvut.cz
<br>
https://gitlab.fit.cvut.cz/molnaben/video-framework

### Assignment
----
The aim of the work is to design a framework for pipeline video processing in order to identify people and their facial expressions of emotions. Design a Python configurable and modular framework for real-time video processing and analysis and storing the results in a database. Using appropriate image processing packages, implement modules for retrieving video from a camera or file, buffering it, recognizing faces in the image, identifying them based on the stored photos, estimating the expression of emotions in faces, exposing the analysis results via HTTP in JSON format, and storing them in a selected database. Implement a web application for configuration and real-time visualization of the analysis process. Document the framework.

### Short description
----
I accomplished to create a modular framework which can build a pipeline with active buffering. We have a processing unit based on a plugin architecture which lets us to extend the processing with any algorithm if the plugin is based on our plugin template. I successfully integrated the face and emotion analyzer package given by the assigner. That analyzer got an extension plugin which saves the output data from the analyzer to a database. I created a web application using the framework to create an interface for easy setup and configuration. This way anybody without any coding knowledge can set up a pipeline. The application has open connections which are able to return raw data in JSON format, this way we enable to further applications to connect on it. If the stream has an active database connection the application has a function to visualize the data.

### VizEmo package
----
The projects analyzation is based on the package VizEmo which contains an emotion and face identifier algorithm. In case if you are interested in using it please contact the supervisor by email: jan.hejda@cvut.cz

The package needs to be placed in the folder: .\\src\\plugins
You will need the edited package where the absolute paths in the package supports this superstructure.

#### Reguirements
----
Please before use, install all the required packages with: pip install -r .\\src\\requirements.txt

**BE AWARE THAT THE VIZEMO(SEE ABOVE) PACKAGE HAS ITS OWN REQUIREMENTS, WHICH YOU WILL NEED TO INSTALL BEFORE USE** 
(Unfortunately creating a virtual environment won't work due to external connections in some of the packages.)

To use the database logger extension - you will need to have an SQL database.
You can set up the login credentials for the database by editing the JSON file in .\\src\\plugins\\login.json


## Guide for use
To launch the application please enter the root directory (which contains the 'src' and 'documentation' folders) from where you are going to launch the command: python .\\src\\app.py
You will be able to reach the application from the browser at: localhost:5000

Setting up a pipeline:
1. Add a camera on the camera page.
	- If you have a webcamera available, usually they have an integer ID of 0 (or 1)
	- To read a video file, the source is the path to the file, and the type is str
2. Add a plugin manager on the plugins page.
3. On the output page select the created camera and the manager, click add process.

### Setting up a MySQL database

To set up a local MySQL database please follow the tutorial here( Windows, Linux, MacOS): https://www.prisma.io/dataguide/mysql/setting-up-a-local-mysql-database

## Available plugins
- **blackNwhite** - translates the frames to grayscale
- **csvWriter** - exports the output data from the analyzer to a CSV file in the folder 'fileOutput'
- **dbLoggerForFaceAnalyzer** - averaging and logging the output data from the analyzer in case there is an active connection and active analyzer. It has two modes: Largest face - to average the emotion for the largest face, By Identity - averages the faces by their identity
- **faceAnalytics** - analyzation plugin using the VizEmo package, can be used with or without face identification
- **inverFrame** - inverts the colors in the frame
- **liveFrame** - ability to display the frame between states through the data channel - in the selected plugin manager, 'Get data'

## Notes
 - To launch the application you will need to have installed the requirements
 - To use the analyzer plugin you must have installed the VizEmo package with the correct paths, and installed all its requirements
 - To use the visualization and database logging features you will need to have a MySQL database set up, and filled out the login.json in the plugins folder
 - Processing from file source is a lot faster than from camera. If the pipeline processed the whole file it will close the stream. **BE AWARE**
 - Sometimes the camera cant be started fast enough and the streams calls a timeout. Please try to start the stream again.

## Features
 - It supports multiple cameras with multiple sources
 - We can change the input configuration of the camera
 - In case of an active stream we can modify the buffers limit and mode
 - We can set up multiple plugin managers with multiple different plugin combinations
 - We can change the plugins supported configuration
 - We can modify the plugin order for different results
 - It supports three output modes
 - We can create plugins, even which takes input parameters - we can define them at starting the process

## Testing
We are using PyTest to automatise the testing and use cases to test the application.
To launch the tests please enter the folder: .\\src\\test , and launch the command: pytest .
This will run all the available test.
You can find all the use cases under the folder: .\\src\\test\\usecases

## Documentation

### View the documentation
In the root folder we can find a "see_doc.bat" file. Launch it to reach the documentation.
The generated documentation is under the folder: .\\documentation\\build\\html

### Re-generate the documentation
We are using Sphinx to generate the documentation from the docstrings.
If you would like to re-generate the documentation:
1. Enter the folder: .\\documentation
3. In the command prompt launch the command: sphinx-apidoc -o .\\source ..\\src\\
2. In the command prompt launch the command: make html

