src
===

.. toctree::
   :maxdepth: 4

   app
   buffer
   bufferManager
   camera
   cameraManager
   databaseManager
   jsonEditor
   outputManager
   pluginCollector
   pluginManager
   plugins
   presets
