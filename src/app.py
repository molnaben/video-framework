import json
import os
from flask import Flask, redirect, render_template, request, flash

from bufferManager import BufferManager
from pluginCollector import PluginCollector
from cameraManager import CameraManager
from outputManager import OutputManager
from pluginManager import PluginManager


app = Flask(__name__)
app.secret_key = 'rjnkljnfhjklnjkn'

cameraHandler = CameraManager()
bufferManager = BufferManager()
pluginCollector = PluginCollector()
outputHandler = OutputManager(cameraHandler,bufferManager,pluginCollector)

#******************************************
#==============INDEX PAGE==================
#******************************************


@app.route('/')
def index():
    """Rendering the template of the index page."""
    return render_template('index.html')


#******************************************
#==============CAMERA PAGE=================
#******************************************

@app.route('/camera')
def camera():
    
    """Rendering the template of the camera page."""
    
    list = cameraHandler.getDictData()
    return render_template('camera.html', list=list)

@app.route('/camera/add', methods=["POST"])
def addCamera():
    
    """POST Url which allows us to add a new camera. We get the name source and format from the form."""
    
    if request.method == 'POST':
        name = request.form['name']
        source = request.form['source']
        format = request.form['format']
        formatted = ""
        if( format == "int"):
            try:
                formatted = int(source)
            except:
                flash("Cant be interpreted as integer")
                return redirect('/camera')
        else:
            formatted = source
        cameraHandler.addNew(formatted, name)
    return redirect('/camera')

@app.route('/camera/delete/<int:id>')
def deleteCamera(id):
    """Deleting the camera with the given ID."""
    if -1 == cameraHandler.delete(id):
        flash("Camera could not be deleted: Wrong ID or camera is currently in use.")
        return redirect('/camera')
    flash("Camera successfully deleted.")
    return redirect('/camera')

@app.route('/camera/update/<int:id>', methods=['GET','POST'])
def updateCamera(id):
    
    """Gets the current values of the camera and offers them for a change. We can change the name and the source through this URL."""
    
    basepath = "src\\presets\\"
    
    if not cameraHandler.isAdded(id) or cameraHandler.getStatusOf(id):
        flash("Camera can not be updated: Wrong ID or camera is currently in use.")
        return redirect('/camera')
    
    if request.method == "GET":
        all = os.listdir(basepath)
        list = []
        for i in all:
            if i.endswith(".json"):
                list.append(i)
        path = cameraHandler.getConfigOf(id)
        if path == -1:
            flash("Camera with id does not exist.")
            return redirect('/camera')
        optDefault = path.replace(basepath, '')
        return render_template('cameraUpdate.html', id=id, name=cameraHandler.getNameOf(id), source=cameraHandler.getSourceOf(id), options=list, optDefault=optDefault)
    
    name = request.form['name']
    source = request.form['source']
    config = request.form['config']
    format = request.form['format']
    formatted = ""
    if( format == "int" ):
        try:
            formatted = int(source)
        except:
            flash("Cant be interpreted as integer")
            return redirect('/camera')
    else:
        formatted = source
    cameraHandler.update(id, name, formatted)
    cameraHandler.setConfig(id, basepath+config)
    
    flash("Camera successfully updated.")
    return redirect('/camera')


#*******************************************
#============BUFFER PAGE====================
#*******************************************

    
@app.route('/buffer')
def buffer():
    """Rendering the template of the buffer page."""
    list = bufferManager.listActiveInfo()
    return render_template('buffer.html', list=list)

@app.route('/buffer/edit/<int:id>', methods=['GET','POST'])
def bufferEdit(id):
    
    """GET and POST URL. Getting the current setup of the selected buffer and offering a change. We can change their limit and mode."""
    
    if request.method=='GET':
        mode, limit = bufferManager.getSetup(id)
        return render_template('bufferEdit.html', mode=mode, limit=limit, id=id)
    if request.method=='POST':
        limit = request.form['limit']
        mode = request.form['mode']
        bufferManager.setBuffer(id,mode,int(limit))
    return redirect('/buffer')


#*******************************************
#============OUTPUT PAGE====================
#*******************************************

@app.route('/output', methods=['GET'])
def output():
    
    """Rendering the template of the output page."""
    
    actives = outputHandler.getActiveOutputs()
    cameras = cameraHandler.getDictData()
    managers = pluginCollector.getList()
    return render_template('output.html', actives=actives, cameras=cameras, managers=managers)

@app.route('/output/add', methods=['POST'])
def outputAdd():
    
    """POST URL to get the data from the form to set up and initialize the new stream."""
    
    cam = request.form['camera']
    managerId = request.form['plugin']
    
    if managerId== "None" or cam == "None":
        flash("Please select a camera and a plugin manager.")
        return redirect('/output')
    
    mode = request.form['mode']
    args = request.form['args']
    args = "{" + args + "}"
    args = json.loads(args)
    
    ret = outputHandler.start(int(mode),int(cam),args,managerId)
    if ret == -1:
        flash("Something went wrong.")
    if ret == -2:
        flash("Selected camera is already live.")
    if ret == -3:
        flash("Manager with this ID does not exist.")
    if ret == -4:
        flash("Camera with this ID does not exist.")
    if ret == -5:
        flash("Missing arguments.")
    return redirect('/output')
    
@app.route('/output/stop/<int:id>')
def outputStop(id):
    
    """URL to send a stop signal for the selected output stream."""
    
    outputHandler.quit(int(id))
    return redirect('/output')


#*******************************************
#============PLUGINS PAGE====================
#*******************************************

@app.route('/plugins')
def plugins():
    
    """Rendering the template of the plugins page."""
    
    input = {}
    list = pluginCollector.getList()
    
    for i in list:
        active = list[i].getList()
        names = []
        for x in active:
            names.append( (x, active[x]) )
        input[i] = names
        
    return render_template('plugins.html', list=input)

@app.route('/plugins/add', methods=['POST'])
def pluginsAdd():
    
    """POST URL to get the name of the plugin manager so it could be added to the collector."""
    
    name = request.form['name']
    ret = pluginCollector.addManager(name)
    if ret == -1:
        flash("Plugin manager with this name already exists.")
    return redirect('/plugins')

@app.route('/plugins/edit/<string:name>')
def managerEdit(name):
    
    """URL to reach the selected plugin manager identified by its name"""
    
    basepath = "src\\plugins\\"
    all = os.listdir(basepath)
    
    manager = pluginCollector.getManager(name)
    
    if manager == -1:
        flash("Manager with name does not exist.")
        return redirect('/plugins')
    
    plugins = manager.getList()
    optionsList = []
    
    for i in all:
        if ".py" in i and i!="__init__.py" and i!="pluginTemplate.py":
            optionsList.append(i)
    
    return render_template('manager.html', list=plugins, name=name, plugins=optionsList)

@app.route('/plugins/edit/<string:name>/add', methods=['POST'])
def addPluginToManager(name):
    
    """POST URL to get the name of the plugin so it could be added to the correct plugin manager."""
    
    plugin = request.form['plugin']
    
    if(plugin == None):
        flash("No valid plugin selected.")
        return redirect('/plugins/edit/'+ name)
    
    plugin = "." + plugin.replace(".py",'')
    
    if(pluginCollector.getManager(name).registerPlugin(plugin) == -1):
        flash("Unsuccessfull plugin registration.")

    return redirect('/plugins/edit/'+ name)

@app.route('/plugins/edit/<string:name>/moveup/<int:id>')
def pluginMoveUp(name, id):
    
    """Moving Up a plugin in the selected plugin manager."""
    
    pluginCollector.moveUp(name, id)
    return redirect('/plugins/edit/'+ name)

@app.route('/plugins/edit/<string:name>/movedown/<int:id>')
def pluginMoveDown(name, id):
    
    """Moving down a plugin in a selected plugin manager."""
    
    pluginCollector.moveDown(name, id)
    return redirect('/plugins/edit/'+ name)

@app.route('/plugins/edit/<string:name>/<int:id>', methods=['GET', 'POST'])
def pluginEdit(name,id):
    
    """GET AND POST URL to get the settings offer from the selected plugin. In the form we can edit them and post them back for the change."""
    
    if request.method == 'GET':
        
        params = pluginCollector.editPlugin(name,id)
    
        if params == -1:
            flash("Cant reach the plugin on the given path.")
            return redirect('/plugins/edit/'+ name)
             
        return render_template('pluginDetails.html', params=params, name=name, id=id)
        
    if request.method == 'POST':
        list = request.form.to_dict(flat=True)
        
        ret = pluginCollector.updatePlugin(name, id, list)
        if ret == -1:
            flash("Something went wrong with the update.")
        else:
            flash("Update successfull.")
            
        return redirect('/plugins/edit/'+ name)
    
    
def getDataFetcher(manager, plugins, name):
    
    """Function to fetch all the available data from all the active plugins in the selected plugin manager."""
    
    data = {}
    
    for plugin in plugins:
        if plugins[plugin] == name:
            try:
                active = manager.getActive()
                ret = active[plugin].getData()
                if ret != -1:
                    data = ret
            except:
                continue
    
    return data

@app.route('/getdata')
def getAllData():
    
    """URL to get RAW data from all the plugin managers."""
    
    all = {}
    
    managers=pluginCollector.getList()
    
    for manager in managers:
        plugins=managers[manager].getList()
        all[manager]=getDataFetcher(managers[manager], plugins, "Database data logger for analysation data")
                
    return json.dumps(all)

@app.route('/getdata/<string:name>')
def getData(name):
    
    """URL to get all data from one specified manager."""
    
    manager : PluginManager = pluginCollector.getManager(name)
    
    if manager == -1:
        return {}
    
    plugins = manager.getList()
    return json.dumps(getDataFetcher(manager,plugins, "Database data logger for analysation data"))

@app.route('/live/<string:name>')
def live(name):
    
    """URL to get live data from the analyser at a selected manager."""
    
    manager : PluginManager = pluginCollector.getManager(name)
    
    if manager == -1:
        flash("Manager with name does not exist.")
        return redirect('/plugins')
        
    return render_template('data.html', name=name)
    
@app.route('/plugins/data/<string:name>/<int:id>')
def sepcial(name, id):
    
    """Get information from the data line from a selected plugin in a specified manager."""
    
    manager : PluginManager = pluginCollector.getManager(name)
    if manager == -1:
        flash("Manager with name does not exist.")
        return redirect('/plugins/edit/'+name)
    
    plugins = manager.getActive()
    
    if id not in plugins:
        flash("Plugin does not exist in this manager.")
        return redirect('/plugins/edit/'+name)
    
    data = plugins[id].getData()
    
    if data == -1:
        flash("This plugin does not support data channel.")
        return redirect('/plugins/edit/'+name)
    
    return data
        
if __name__ == "__main__":
    app.run(debug=True)