import queue
from camera import Camera
import jsonEditor

class Buffer:
    """
    The buffer serves the purpose to hold the frames coming from the source until they get processed.
    We have a seperate function running on a different thread to fill in the buffer, discussed in the buffer manager.
    :param queue: We are using queue.Queue to store the frames. We are using this type of container because of its LIFO and thread lock.
    :type config: Queue
    
    :param amount: Current number of frames in the buffer.
    :type amount: int
    
    :param camera: Connection to the paired camera.
    :type camera: Camera
    
    :param mode: Current mode of the buffer.
    :type mode: str
    
    :param limit: Maximum amount of frames in the buffer.
    :type limit: int
    """
        
    def __init__(self, camera : Camera):
        self.queue = queue.Queue()
        self.amount = 0
        self.camera = camera
        self.mode = "throw-old"
        self.limit = 128
        
        ret = self.readConfig("src\\buffer_default.json")
        if ret == -1:
            print("Buffer could not be initialised. Using default configuration: throw-old, limit: 128")
        
        
    def readConfig(self, name):
        """
        Sets the configuration for the buffer from a configuration file. 

        Args:
            :param name: Contains the path to the configuration file.
            :type config: str

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        data = jsonEditor.read(name)
        if(data == -1):
            print("Buffer could not be initialised, missing or corrupt defualt setup file.")
            return -1
        self.limit = int( data["buffer_size"] )
        ret = self.setMode( data["buffer_type"] )
        if(ret == -1):
            print("Buffer could not be initialised, missing or corrupt defualt setup file.")
            return -1
        print(f"Buffer configuration is loaded. Type: {self.mode} Limit: {self.limit}")
        return 1

    def requestFrame(self):
        """
        Requests a frame from the connected camera. In case we got a frame it processes it by the current mode.
        Mode infinity allows us to fill the buffer without a limit.
        While throw-new throws the gotten frame if the buffer is full.
        The throw-old mode pops the oldest frame and adds the new one.

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        
        ret, frame = (self.camera.getFrame())
        if not ret :
            return -1
        
        if( self.mode == "infinity"):
            self.queue.put( frame )
        if( self.mode == "throw-new"):
            if(self.amount >= self.limit):
                frame
            else:
                self.queue.put( frame )
                self.amount += 1
        if( self.mode == "throw-old"):
            if(self.amount >= self.limit):
                self.popFrame()
            self.queue.put(frame)
            self.amount += 1
        
        return 1

    def popFrame(self):
        """
        Returns a frame from the buffer if there is a frame to return.
        In case the queue.get timeouts it returns a negative answer.

        Returns:
            :return: Returns 1 and the frame in case of correct run. Returns -1 and [] in case something went wrong.
        """
        
        if( self.mode != "infinity" ):
            self.amount -= 1
        if( self.amount < 0 ):
            self.amount = 0
            
        try:
            return 1, self.queue.get(timeout=1)
        except:
            return -1, []

    def clear(self):
        """
        Clears all the frames from the buffer.
        """
        while not self.queue.empty():
            self.queue.get()
            self.amount -=1
            
    def setLimit(self, limit):
        self.limit = limit
    
    def getLimit(self):
        return self.limit
    
    def getMode(self):
        return self.mode
            
    def setMode(self, mode):
        """
        Checks the input and pairs it with the mode type then sets the mode.

        Args:
            :param mode: Input from the user or other functions regarding the mode of the buffer.
            :type mode: str

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        if( "infinity" in mode ):
            self.mode = "infinity"
        elif( "throw-new" in mode):
            self.mode = "throw-new"
        elif( "throw-old" in mode):
            self.mode = "throw-old"
        else:
            print(f"Mode {mode} for buffering does not exist!")
            return -1
        return 1