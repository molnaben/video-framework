from threading import Thread
from camera import Camera
from buffer import Buffer

class BufferManager:
    
    """
    Buffer managers holds and manages all the active buffers and buffer filling threads.
        
    :param liveCameraThreads: Class attribute to hold all the active threads paired with the buffer differentiated with the used camera ID.
        
    """
    
    liveCameraThreads = {}
    
    def __del__(self):
        for key in self.liveCameraThreads:
            self.liveCameraThreads[key][0].join()
            self.liveCameraThreads.pop(key)
            
    def listActiveCameras(self):
        """
        Listing the active cameras by their IDs
        Returns:
            :return: IDs of the cameras.
        """
        return self.liveCameraThreads.keys()
    
    def listActiveInfo(self):
        """Getting more information about the buffers itself.

        Returns:
            :return: Dictionary by the camera ID holding the information about the buffer mode and its limit.
        """
        ret = {}
        for key in self.liveCameraThreads:
            ret[key] = [self.liveCameraThreads[key][1].getMode(), self.liveCameraThreads[key][1].getLimit()]
        return ret
    
    def __frameGetterThread(self, camera: Camera, buffer: Buffer):
        """This function is launched on a seperate thread to fill in the connected buffer from the selected camera.
            The collecting stopes if no input arrives from the camera.
        Args:
            :param camera: The camera to get the frames from.
            :type camera: Camera
            
            :param buffer: The buffer to be filled up.
            :type buffer: Buffer
        """
        print(f"Camera {camera.getName()} is collecting data!")
        while camera.getStatus():
            ret = buffer.requestFrame()
            if ret == -1:
                break
        print(f"Camera {camera.getName()} stopped collecting data!")
            
    def setCameraLive(self, camera: Camera):
        """Setting camera live means activating the camera, creating a buffer and creating a thread for collecting the frames.

        Args:
            :param camera: The selected camera to request the frames from.
            :type camera: Camera

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        cameraId:int = camera.getId()
        
        if camera is None:
            print("Missing camera with ID: " + str(cameraId))
            return -1
        
        if cameraId in self.liveCameraThreads:
            print("Camera with ID: " + str(cameraId) + " is already LIVE!")
            return -1
        
        camera.start()
                
        buff : Buffer = Buffer(camera)
        self.liveCameraThreads[ cameraId ] = (Thread(target=self.__frameGetterThread, args=[camera,buff]),buff)
        self.liveCameraThreads[ cameraId ][0].start() 
        
        print(f"Camera {cameraId} frame collecting started!")
        return 1
        
    def quitLive(self, camera: Camera):
        """Quitting the process means stopping the camera which stops the frame collecting.
        Then we join the thread and pop the listing from the queue.

        Args:
            :param camera: The selected camera to stop
            :type camera: Camera
            
        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        
        cameraId:int = camera.getId()
            
        if cameraId not in self.liveCameraThreads:
                print("Camera with ID: " + str(cameraId) + " is currently not LIVE!")
                return -1
        
        camera.isInUse = False
        
        self.liveCameraThreads[ cameraId ][0].join()
        self.liveCameraThreads.pop(cameraId)
        
        camera.isInUse = True
        camera.stop()
        return 1
        
    def getFrameFromId(self, id: int):
        """Requesting a frame from the selected buffer.

        Args:
            :param id: Defines the selected buffer.
            :type id: int

        Returns:
            :return: Returns 1 and the frame in case of correct run. Returns -1 and empty [] in case something went wrong.
        """
        
        if id in self.liveCameraThreads:
            ret, frame = self.liveCameraThreads[id][1].popFrame()
            if ret == -1:
                return -1,[]
            return 1,frame
        
        print("Camera with ID: " + str(id) + " is currently not LIVE!")
        return -1,[]
    
    def getSetup(self, id):
        if id not in self.liveCameraThreads:
            return -1,-1
        return self.liveCameraThreads[id][1].getMode(), self.liveCameraThreads[id][1].getLimit()
    
    def setBuffer(self, id, mode, limit):
        if id not in self.liveCameraThreads:
            return -1
        self.liveCameraThreads[id][1].setLimit(limit)
        return self.liveCameraThreads[id][1].setMode(mode)


