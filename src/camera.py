
import cv2
import jsonEditor


class Camera:
    """
    This is class represening the source and the operations with the source.
    It estabilishes a connection with the source, from where we are able to get frames.
    
    :param id: Identification number of the camera to simplify referencing the object.
    :type id: int
    
    :param name: The user defines a name for the camera to be more recognisable.
    :type name: str
    
    :param recorder: Instance of the CV2 video recorder.
    
    :param isInUse: Defines if the camera is active currently or not. True if active false if not.
    :type isInUse: Boolean
    
    :param source: Path for the source from where we are getting the frames. Can be an IP, file path or connected camera.
    
    :param configPath: Path for the configuration path/preset.
    """
    
    def __init__(self, id, name, source):
        """
        Costructor method.
        """
        self.id = id
        self.name = name
        self.recorder = None
        self.isInUse = False
        self.source = source
        self.configPath = "src\\presets\\720p.json"
        
    def getId(self):
        return self.id

    def getName(self):
        return self.name
    
    def getStatus(self):
        return self.isInUse
    
    def getSource(self):
        return self.source
    
    def update(self, name, source):
        self.name = name
        self.source = source
        
    def setConfigPath(self, path):
        self.configPath = path
        
    def getConfigPath(self):
        return self.configPath
    
    def setConfig(self, config):
        """
        Sets the configuration for the recorder from a preset file. 

        Args:
            :param config: Contains the path to the preset file.
            :type config: str

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        data = jsonEditor.read(config)
        if( data == -1 ):
            print("Setting configuration failed")
            return -1
        try:
            if 'width' not in data or 'height' not in data:
                raise Exception("Invalid parameters from config file.")
            self.recorder.set(cv2.CAP_PROP_FRAME_WIDTH, int(data['width']))
            self.recorder.set(cv2.CAP_PROP_FRAME_HEIGHT, int(data['height']))
        except:
            print("Camera is not started. Cant set configuration.")
            return -1
        print(f"Configuration of camera {self.name} is set.")
        return 1

    def start(self):
        """
        Starts the camera if its currently offline. It creates the cv2 VideoCapture recorder and calls the function to set the configuration.
        
        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        if( self.isInUse == False ):
            try:
                self.recorder = cv2.VideoCapture(self.source) # , cv2.CAP_DSHOW
            except:
                print(f"{self.name} could not be started!")
                return -1
            self.isInUse = True
            self.setConfig(self.configPath)
            return 1
        else:
            print(f"{self.name} is already in use!")
            return -1
        
    def stop(self):
        """
        In case the camera is active, its disables it and releases the recorder.
        
        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        if self.isInUse == True:
            self.isInUse = False
            self.recorder.release()
            return 1
        else:
            print(f"{self.name} is currently not in use!")
            return -1
    
    def getFrame(self):
        """
        This method is called if we would like to get a frame from the camera.
        It contains an iteration which tries to get a frame.

        Returns:
            :return: Returns 1 and a frame from the source in case of correct run. Returns -1 and [] in case something went wrong.
        """
        for i in range(0,60): #RETRY UNTIL TIMEOUT
            ret, frame = self.recorder.read()
            if ret:
                return ret, frame
        print(f"No input arrives from {self.name} ")
        return ret, []
                