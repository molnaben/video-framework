from typing import Dict
from camera import Camera

class CameraManager:
    """Manages the registration of the sources-cameras.
    
    :param numberOfCameras: Total number of the registered cameras.
    :type numberOfCameras: int
    
    :param sources: Holds the registered cameras connected with an ID.
    :type sources: Dict
    """
    
    numberOfCameras = 0
    sources: Dict[int, Camera] = {}
        
    def addNew( self , input, name):
        """Registering a new camera.

        Args:
            :param input: The source. Can be a connected camera referenced with an integer, a path to a file or an IP address to an IP camera.
            
            :param name: Name for the camera to help the user identify the needed camera.

        Returns:
            :return: Returns the created camera.
        """
        cam = Camera( self.numberOfCameras, name, input )
        self.sources[self.numberOfCameras] = cam
        self.numberOfCameras += 1
        
        print(f"Camera {name} succesffully added!")
        return cam
        
    def delete(self, id):
        """Delete the camera with the ID.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns 1 if successfull, -1 if not.
        """
        if self.isAdded(id) and not self.sources[id].getStatus():
            self.sources.pop(id)
            return 1
        return -1
    
    def update(self, id, name, source):
        """Updating the name and source of the camera.
        
        Args:
            :param id: ID of the camera
            :type name: int
            
            :param name: Name to change to.
            :type name: str
            
            :param source: The path to change to.
            :type source: str/int

        Returns:
            :return: Returns 1 if successfull, -1 if not.
        """
        if self.isAdded(id) and not self.sources[id].getStatus():
            self.sources[id].update(name, source)
            return 1
        return -1
    
    def setConfig(self,id, path):
        """Setting the configuration path of a camera.
        
        Args:
            :param id: ID of the camera
            :type name: int
            
            :param path: The path to change to.
            :type path: str

        Returns:
            :return: Returns 1 if successfull, -1 if not.
        """
        if self.isAdded(id) and not self.sources[id].getStatus():
            self.sources[id].setConfigPath(path)
            return 1
        return -1
    
    def getConfigOf(self, id):
        """Getting the configuration of a camera.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns the config path if the camera exists, -1 if not.
        """
        if self.isAdded(id):
            return self.sources[id].getConfigPath()
        return -1
    
    def isAdded(self, id):
        """Checking if a camera exists.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns true if the camera exists, false if not.
        """
        return id in self.sources
    
    def getStatusOf(self, id):
        """Getting the current status of a camera.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns true or false if the camera exists, -1 if not.
        """
        if self.isAdded(id):
            return self.sources[id].getStatus()
        return -1
    
    def getNameOf(self, id):
        """Getting the name of a camera.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns the name if the camera exists, -1 if not.
        """
        if self.isAdded(id):
            return self.sources[id].getName()
        return -1
    
    def getSourceOf(self, id):
        """Getting the source of a camera.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns the source if the camera exists, -1 if not.
        """
        if self.isAdded(id):
            return self.sources[id].getSource()
        return -1
    
    def getCamByID(self, id)->Camera:
        """Getting the camera by its ID.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns the camera if it exists, -1 if not.
        """
        if self.isAdded(id):
            return self.sources[id]
        return -1
    
    def getFrame(self, id):
        """Getting the frame from a camera.
        
        Args:
            :param id: ID of the camera
            :type name: int

        Returns:
            :return: Returns the frame if the camera exists, -1 if not.
        """
        if self.isAdded(id):
            return self.sources[id].getFrame()
        return -1
    
    def getCamByName(self, name):
        """Getting the camera by its name.

        Args:
            :param name: Name of the camera
            :type name: str

        Returns:
            :return: Returns the camera if exists, -1 if not.
        """
        for i in self.sources:
            if self.sources[i].getName() == name:
                return self.sources[i]
        return -1
    
    def getDictData(self):
        """Getting data regarding the cameras in a dictionary form.

        Returns:
            :return: Dicrionary with the camera IDs as keys holding a touple of the name and the source.
        """
        dict = {}
        for i in self.sources:
            dict[i] = (self.sources[i].getName(),self.sources[i].getSource())
        return dict
        
    def listCams(self):
        """Listing the cameras in a paired form.

        Returns:
            :return: Returns a list of pairs with ID and the name of the camera.
        """
        list = []
        for i in self.sources:
            list.append( str( str( i ) + " : " + str( self.sources[i].getName() ) ) )
        return list