
from datetime import datetime
import jsonEditor
import mysql.connector

class DatabaseManager:
    """A central database manager to allow the plugins and other modules to make and hold conncection with databases.
    
    :param data: Content of the login file.
    :type data: str
    
    :param hostName: Hostname of the server.
    :type hostName: str
    
    :param name: Login name of the server.
    :type name: str
    
    :param password: Password of the server.
    :type password: str
    
    :param database: Database name we would like to use.
    :type hostName: str
    
    :param connection: Instance of the current connection.
    
    :param login: path of the login file.
    :type hostName: str
    """
    
    def __init__(self, loginFile) -> None:
        data = jsonEditor.read(loginFile)
        if(data == -1):
            raise Exception("Database could not be initialised, missing or corrupt defualt setup file.")
        
        self.hostName = data['host']
        self.name = data['login']
        self.password = data['pwd']
        self.connection = None
        self.database = None
        self.login = loginFile
        
        if 'database' in data:
            self.database = data['database']
                
        try:
            self.connection = mysql.connector.connect(host=self.hostName, user=self.name, passwd=self.password)
            print("MYSQL logged in to server.")
            
        except:
            raise Exception("Couldnt connect to the server")
        
    def __del__(self):
        self.connection.close()
        
    def createDatabase(self, name):
        
        """Creates a database with the given name and saves the credentials to the original file.

        Args:
            :param name: The name of the database that should be created.

        Raises:
            :raises Exception: Raising an exception the database cant be created or the credentials couldnt be written to the file.
        """
        
        cursor=self.connection.cursor()
        try:
            cursor.execute(f"create database if not exists {name}")
            print(f"Database {name} created successfully.")
            data = jsonEditor.read(self.login)
            if(data == -1):
                raise Exception("Login file could not be read and modified.")
            data['database']=name
            self.database = name
            jsonEditor.write(self.login,data)
            #cursor.close()
        except:
            raise Exception("Database cant be created.")
        
    def connectDatabase(self,name=None):
        
        """Creates a database connection with the given name. If the database does not exist it calls the function the create a new one.

        Args:
            :param name: The database name we should connect to. Defaults to None.
            :type name: str

        Raises:
            :raises Exception: In case it couldnt connect to the database.
        """
        
        if name == None:
            name = self.database
        try:
            self.connection = mysql.connector.connect(host=self.hostName, user=self.name, passwd=self.password, database = name)
            self.database=name
            print("MYSQL logged in to database.")
        except:
            try:
                self.createDatabase(name)
                self.connection = mysql.connector.connect(host=self.hostName, user=self.name, passwd=self.password, database = name)
                self.database=name
                print("MYSQL logged in to database.")
            except:
                raise Exception("Couldnt connect to the database")
        
    def execute(self, query,commit=True, values = None):
        
        """Allows executing SQL queries through the connection.
        We are able to define if we would like to commit or not or multiple values in the query.

        Args:
            :param query: The commands we would like to execute.
            :param commit: Boolean to define if we would like to commit or not. Defaults to True.
            :param values: Optionally we can define multiple values for the commands. Defaults to None.

        Returns:
            :return: Returns the used cursor in case we would like to fetch data.
        """
        
        cursor = self.connection.cursor()
        now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        try:
            if( values != None ):
                cursor.execute(query, values)
            else:
                cursor.execute(query)
            if commit:
                self.connection.commit()
            print(f"{now} - Commands executed and/or committed.")
            return cursor
        except:
            print(f"{now} - Commands could not be executed and/or committed.")
            return cursor
