import json

def read(name):
    """Handles the reading of a JSON file.

    Args:
        :param name: Input path for the file to be read.

    Returns:
        :return: Returns the read data. In case somethings go wrong it returns a -1.
    """
    data = []
    try:
        with open(name, "r") as jsonfile:
                data = json.load(jsonfile)
        print("JSON file reading successful")
        jsonfile.close()
        return data
    except:
        print("Couldn't read the file(\"" + name +"\")")
        return -1
    
def write(name, data):
    """Handles the writing of a JSON file.

    Args:
        :param name: The filename and path to be written to.
        :param data: The data we would like to write into the file.

    Returns:
        :return: Returns 1 in case of success, returns -1 if something goes wrong.
    """
    try:
        with open(name, "w") as jsonfile:
            json.dump(data,jsonfile) 
        print("JSON file writing successful")
        jsonfile.close()
        return 1
    except:
        print("Couldn't write the file(\"" + name +"\")")
        return -1