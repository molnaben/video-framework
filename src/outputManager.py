from camera import Camera
from cameraManager import CameraManager
from bufferManager import BufferManager
from threading import Thread
import cv2

from pluginCollector import PluginCollector

class OutputManager:
    """Serves the purpose to connect all the managers in the cores system, and to launch the process.
    Currently it can be launched in three modes. Live view, save to file and only process.
    
    :param camManager: Instance of the camera manager.
    :param bufManager: Instance of the buffer manager.
    :param pluginCollector: Instance of the plugin collector.
    :param outputs: Dictionary to keep track of the currently active streams.
    """
    
    def __init__(self, camManager:CameraManager, bufferManager: BufferManager, pluginCollector:PluginCollector):
        self.camManager = camManager
        self.bufManager = bufferManager
        self.outputs : dict={}
        self.pluginCollector = pluginCollector
        
    def __del__(self):
        for key in self.outputs:
            self.outputs[key][1].join()
            self.outputs.pop(key)
            
    def getActiveOutputs(self):
        """Getting a dictionary of the active active outputs for displaying purposes.

        Returns:
            :return: Returns a dictionary with the mode and the plugin managers name.
        """
        ret = {}
        for i in self.outputs:
            ret[i]=[self.outputs[i][2],self.outputs[i][0].getName()]
        return ret
    
    def liveView( self , cameraId : int , args:dict):
        """The live view mode displays the frame in a window on the host computer after processing it.
        The live view mode can be used for controlling the input or to monitor the footage.
        It sets up the whole process from setting up the camera, pairing it with a buffer and processing the data.

        Args:
            :param cameraId: The chosen camera ID to get the frames from.
            :type cameraId: Camera
            
            :param args: All initial arguments can be passed into the processing unit through this dictionary.
            :type args: Dict
        """
        
        camera : Camera = self.camManager.getCamByID(cameraId)
        
        self.bufManager.setCameraLive(camera)
        
        windowname = camera.getName() + " LiveView"

        cv2.namedWindow(windowname)

        while( camera.getStatus() ):
            
            tmp_arg = args.copy()
            
            code, frame = self.bufManager.getFrameFromId(cameraId)
            
            if code == -1:
                self.quit(cameraId,False)
                break
            
            tmp_arg['frame']=frame
            self.outputs[cameraId][0].useAll(tmp_arg)
            
            cv2.imshow(windowname, tmp_arg['frame'])
            
            if ( cv2.waitKey(1) & 0xFF == ord('q') ):
                   self.quit(cameraId,False)
                   break

        cv2.destroyWindow(windowname)
        
        
    def saveFile( self , cameraId : int , args:dict):
        """The save to file mode exports the last processed frame into the specified file.
        It takes two initial arguments from the args. The filename-path and the FPS count.
        By changing the FPS count we can speed up or slow down the exported video stream.
        If these two parameters are not present the output wont start.
        It sets up the whole process from setting up the camera, pairing it with a buffer and processing the data.
        Args:
            :param cameraId: The chosen camera ID to get the frames from.
            :type cameraId: Camera
            
            :param args: All initial arguments can be passed into the processing unit through this dictionary.
            :type args: Dict
        """
        
        camera : Camera = self.camManager.getCamByID(cameraId)
        
        self.bufManager.setCameraLive(camera)
        
        isOk = False
        h=0
        w=0
        
        for i in range(0,5):
            code, initframe = self.bufManager.getFrameFromId(cameraId)
            try:
                h = len(initframe)
                w = len(initframe[0])
                isOk = True
                break
            except:
                continue
            
        if not isOk:
            self.quit(cameraId,False)
            return
        
        h = len(initframe)
        w = len(initframe[0])
        
        if 'width' in args:
            w = args['width']
            print(f"Width set to {w}.")
        elif 'height' in args:
            h = args['height']
            print(f"Height set to {h}.")
        else:
            print(f"Running on base resolutin {w}x{h}!")
            
        size = (w,h)
        
        print(f"Saving to {args['filename']}.")
        result = cv2.VideoWriter(args['filename'], cv2.VideoWriter_fourcc(*'MJPG'), int(args['fps']), size)
        
        while(camera.getStatus()):
            
            tmp_arg = args.copy()
            
            code, frame = self.bufManager.getFrameFromId(cameraId)
            
            if code == -1:
                self.quit(cameraId,False)
                break
            
            tmp_arg['frame']=frame
            ret = self.outputs[cameraId][0].useAll(tmp_arg)
            tmp_arg = ret
            result.write(tmp_arg['frame'])
            
        result.release()
        
    def process( self , cameraId : int , args:dict):
        """This mode only processes the frames without any output. Can be useful if we save the processed data in the processing units.
        It sets up the whole process from setting up the camera, pairing it with a buffer and processing the data.
        Args:
            :param cameraId: The chosen camera ID to get the frames from.
            :type cameraId: Camera
            
            :param args: All initial arguments can be passed into the processing unit through this dictionary.
            :type args: Dict
        """
        camera : Camera = self.camManager.getCamByID(cameraId)
        
        self.bufManager.setCameraLive(camera)
        
        while(camera.getStatus()):
            
            tmp_arg = args.copy()
            
            code, frame = self.bufManager.getFrameFromId(cameraId)
            
            if code == -1:
                self.quit(cameraId,False)
                break
            
            tmp_arg['frame']=frame
            self.outputs[cameraId][0].useAll(tmp_arg)

    def start(self, mode:int, id:int , args:dict, managerID:int):
        """This functions chooses the correct mode and gets the correct modules for the new pipeline.
        It puts the correct mode on a new thread so we could handle multiple streams at once.

        Args:
            :param mode: Defines the mode type. 1-Live view, 2-Save to file, 3-Only process.
            :type mode: int
            
            :param id: Identification number for the camera.
            :type id: int
            
            :param args: All initial arguments can be passed into the processing unit through this dictionary.
            :type args: Dict
            
            :param managerID: Identification number of the plugin manager.
            :type managerID: int

        Returns:
            :return: Returns 1 if the process run correctly. Returns -N if something went wrong, where N is a number of the correct error code.
        """
        
        if id in self.outputs:
            print(f"Camera with id {id} is already LIVE!")
            return -2
        
        if -1 == self.camManager.getCamByID(id):
            print(f"Camera with ID does not exist.")
            return -4
        
        pluginManager = self.pluginCollector.getManager(managerID)
        
        if -1 == pluginManager:
            print(f"Manager with ID {managerID} does not exist.")
            return -3
        
        function=None
        modeText=''
        
        if(mode == 1):
            function=self.liveView
            modeText = "Live view"
        elif(mode == 2):
            function=self.saveFile
            modeText = "Save to file"
            
            if 'filename' not in args:
                print("Output name not specified!")
                return -5
        
            if 'fps' not in args:
                print("No fps given!")
                return -5
            
        elif(mode == 3):
            function=self.process
            modeText = "Only process"
        else:
            print("Mode does not exist!")
            return -1
        
        self.outputs[id]= [ pluginManager ,Thread( target=function, args=[id,args]), modeText ]
        self.outputs[id][1].start()
        return 1
        
    def quit(self, id: int, join=True):
        """Quitting the output stream and joining the thread.

        Args:
            :param id: ID fo the selected output we would like to stop.
            :type id: int
            
            :param join: Parameter to set if we need a join or not. Defaults to True.
            :type join: Boolean

        Returns:
            _type_: _description_
        """
        cam = self.camManager.getCamByID(id)
        if cam == -1:
            return -1
        ret = self.bufManager.quitLive(cam)
        if ret != -1:
            if join:
                self.outputs[id][1].join()
            self.outputs.pop(id)
            return 1
        return -1