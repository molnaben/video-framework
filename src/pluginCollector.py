
from pluginManager import PluginManager


class PluginCollector():
    """This managers adds and keeps track of the plugin managers.
    For the purpose we could use different plugin managers for different output streams.

    :param pluginManagers: Holds the added plugin managers. They are differentiated by their names given by the user.
    :type pluginManagers: Dict
    """
    
    pluginManagers= {}
    
    def getList(self):
        """
        Returns:
            :return: Returns the class attribute dictionary of the active managers.
        """
        return self.pluginManagers
    
    def addManager(self, name):
        """Adding a manager with the name given by attribute name.

        Args:
            :param name: Defines the name of the new plugin manager.
            :type name: str

        Returns:
            :return: Returns the newly created manager.
        """
        if name in self.pluginManagers:
            print(f"Manager with name {name} already exists.")
            return -1
        self.pluginManagers[name]=PluginManager()
        self.pluginManagers[name].setName(name)
        return self.pluginManagers[name]
    
    def editPlugin(self, name, id):
        if name not in self.pluginManagers:
            return -1
        return self.pluginManagers[name].editPlugin(id)
    
    def updatePlugin(self, name, id, changes):
        if name not in self.pluginManagers:
            return -1
        return self.pluginManagers[name].updatePlugin(id, changes)
    
    def getManager(self, name)->PluginManager:
        if name not in self.pluginManagers:
            return -1
        return self.pluginManagers[name]
    
    def moveUp( self, name, id):
        if name not in self.pluginManagers:
            return -1
        return self.pluginManagers[name].moveUp(id)
    
    def moveDown( self, name, id):
        if name not in self.pluginManagers:
            return -1
        return self.pluginManagers[name].moveDown(id)