import importlib
from threading import Lock

class PluginManager:
    """Plugin managers serves the purpose to hold all the plugins, extensions which are doing the processing.
    Each plugin does its part of processing for example it can analyse the frame, save or process the result data, atd.
    This manager loads and registers the new extensions with importlib. It enables us to extend the processing unit even during the run.

    :param orderNumber: Unique identifier for all the plugins.
    :type orderNumber: int
    
    :param activePlugins: Holding the instances of the active plugins.
    :type activePlugins: Dict
    
    :param block: Lock to keep the dictionary unmodified until one iteration of the processing runs.
    :type block: Lock
    
    :param name: Name of the manager for easy human identification.
    :type name: str
    
    :param registeredAll: Class attribute to keep the links of the loaded plugins to prevent multiple import.
    :type registeredAll: Dict
    """
    
    registeredAll = {}
    
    def __init__(self):
        self.orderNumber = 0
        self.activePlugins : dict = {}
        self.block = Lock()
        self.name = ""
        
    def loadPlugin(self, name):
        """Handles the importmodule import in case something goes wrong.

        Args:
            :param name: Name of the plugin to load.

        Returns:
            :return: Returns the link to the imported extension. In case it couldnt be imported, returns -1.
        """
        try:
            return importlib.import_module(name,".plugins")
        except:
            return -1
    
    def registerPlugin(self, name):
        """Registering the plugin means importing the plugin if its not imported yet and creating a new instance for this object of pluginManager.

        Args:
            :param name: Name of the plugin to load.

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        
        self.block.acquire()
        
        if name not in self.registeredAll:
            manager = self.loadPlugin(name)
            
            if( manager == -1 ) :
                print(f"Couldnt load the plugin {name}!")
                self.block.release()
                return -1
            
            self.registeredAll[name] = manager
            
        manager = self.registeredAll[name]
        
        self.activePlugins[self.orderNumber] = manager.Extension()
        self.orderNumber += 1

            
        print(f"Plugin {name} successfully loaded!")
        self.block.release()
        return 1
        
        
    def moveUp(self,id):
        """Moving up a plugin inside the active plugins. This results the changing of the processing order.
        We can achieve different results with re-defining the extension order.

        Args:
            :param id: ID of the plugin to move one up.

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        if id not in self.activePlugins:
            return -1
        
        self.block.acquire()
        if( id-1 < 0 ):
            self.block.release()
            return 1
        tempLower = self.activePlugins[id-1]
        
        self.activePlugins[id-1] = self.activePlugins[id]
        self.activePlugins[id] = tempLower
        self.block.release()
        return 1
        
    def moveDown(self,id):
        """Moving down a plugin inside the active plugins. This results the changing of the processing order.
        We can achieve different results with re-defining the extension order.

        Args:
            :param id: ID of the plugin to move one down.

        Returns:
            :return: Returns 1 in case of correct run. Returns -1 in case something went wrong.
        """
        if id not in self.activePlugins:
            return -1
        
        self.block.acquire()
        if( id+1 > self.orderNumber-1 ):
            self.block.release()    
            return 1
        tempLower = self.activePlugins[id+1]
        
        self.activePlugins[id+1] = self.activePlugins[id]
        self.activePlugins[id] = tempLower
        self.block.release()
        return 1
        
    def listPlugins(self):
        """Printing a list of the plugins into the console.
        """
        self.block.acquire()
        for key in self.activePlugins:
            print(key, " : ", self.activePlugins[key].getName())
        self.block.release()
            
    def useAll(self, args:dict):
        """This function is called to process the incoming arguments with all the active plugins.
        It iterates through between the plugins and calls the compulsory process function to make the changes.

        Args:
            :param args: Incoming dictionary of arguments. Contains all the information whats needed for the extensions.
            :type args: Dict

        Returns:
            _type_: _description_
        """
        popkey = []
        self.block.acquire()
        for key in self.activePlugins:
            
            try:
                args = self.activePlugins[key].process(args)
            except:
                print(f"Plugin {key} is not compatible with the other active plugins.")
                popkey.append(key)
                continue
        
        for i in popkey:
            self.activePlugins.pop(i)
        self.block.release()
        return args
    
    def getActive(self):
        """
        Returns:
            :return: Returns the dictionary of the active plugins.
        """
        return self.activePlugins
    
    def getName(self):
        """
        
        Returns:
            :return: Returns the name of the manager.
        """
        return self.name
    
    def setName(self,name):
        """Sets the plugin managers name.
        
        Args:
            :param name: The name we would like to set it to.
        """
        self.name = name
    
    def getList(self):
        """Getting the names of all the active plugins.

        Returns:
            :return: Dictionary of all the plugin names by their ID.
        """
        self.block.acquire()
        
        ret = {}
        for i in self.activePlugins:
            ret[i]=self.activePlugins[i].getName()
            
        self.block.release()
        return ret
    
    def editPlugin(self, id):
        """Get all the editable information about the selected plugin.

        Args:
            :param id: ID of the selected plugin.

        Returns:
            :return: Returns -1 if the plugin does not exist. In case of success it returns the dictionary of the configurations.
        """
        if id not in self.activePlugins:
            return -1
        return self.activePlugins[id].edit()
    
    def updatePlugin(self, id, params):
        """Calls the update function in the selected plugin on the input arguments which should be the changed settings.

        Args:
            :param id: ID of the selected plugin.
            :type id: int
            
            :param params: Dictionary of the edited settings.
            :type params: Dict

        Returns:
            :return: Returns -1 in case of the plugin does not exist. Otherwise it returns the return code of the update function.
        """
        if id not in self.activePlugins:
            return -1
        return self.activePlugins[id].update(params)
    