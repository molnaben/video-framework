import csv
from datetime import datetime
from xmlrpc.client import Boolean
from plugins.pluginTemplate import Plugin

probabilityOptions = ['Neutral','Happy','Sad','Surprise','Fear','Disgust','Anger','Contempt']

class Extension(Plugin):
    
    """This plugins serves the purpose to save the analysed data from the face and emotion recogniser to a CSV file.
    
    :param folder: Path to the output folder.
    :param defaultFile: Default file to save to.
    :param frameNumber: Row ID inside the CSV file.
    :param fileSource: Currently used file.
    :param file: The link to the file.
    """
    
    def init(self):
        self.name = "Data logger to CSV File"
        self.folder= "src\\fileOutput\\"
        self.defaultFile = "csvdump.csv"
        self.frameNumber = 0
        self.fileSource = self.defaultFile
        self.file = None
        
        return self.open(self.fileSource)
    
    
    def process(self, args:dict):
        
        """If there is data from the analyser in the arguments it will be saved to the active CSV file line by line."""
        
        if not self.active or 'data' not in args:
            return args
    
        data = args['data']
        
        for face in data['Faces']:
            position = face['Position']
            
            probabData = []
        
            for x in probabilityOptions:
                probabData.append(face['Probabilities'][x])

            now = datetime.now().strftime("%H:%M:%S")
            self.writer.writerow([
                now,
                self.frameNumber,
                face['Identity'],
                face['PredictedEmotion'], face['SecondaryEmotion']] + probabData + [ face['Valence'], face['Arousal'],
                position['x'], position['y'], position['width'], position['height']
            ])
            self.frameNumber += 1
        
        self.file.flush()
            
        return args
        
    
    def open(self, filename):
        
        """Opens and saves the path to the currently active file.

        Args:
            :param filename: The file name and path to be opened.
            :type filename: str

        Returns:
            :return: Returns -1 in case something went wrong.
        """
        
        if self.file is not None:
            self.file.close()
        
        try:
            self.file = open(self.folder + filename, 'w', newline='')
            self.writer = csv.writer(self.file, delimiter=';')
            self.fileSource = filename
            
            self.writer.writerow([
                'Time','FrameNumber', 'Identity', 'PredictedEmotion', 'SecondaryEmotion',
                'Neutral', 'Happy', 'Sad', 'Surprise', 'Fear', 'Disgust', 'Anger', 'Contempt',
                'Valence', 'Arousal', 'x', 'y', 'width', 'height'
            ])
            
            print(f"File {filename} initialised.")
        except:
            print("Something went wrong at opening or writing the CSV file.")
            self.file=self.defaultFile
            return -1
        
    def __del__(self):
        if self.file is not None:
            self.file.close()
            
    def edit(self) -> dict:
        return {'File':self.fileSource, 'Active':self.active}
    
    def update(self, input: dict):
        
        if 'File' not in input:
            return -1

        if input['Active'] == "True":
            self.active=True
        elif input['Active'] == "False":
            self.active=False
        return self.open(input['File'])
        
        