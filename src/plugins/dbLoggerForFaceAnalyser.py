from plugins.pluginTemplate import Plugin
from databaseManager import DatabaseManager

""" 
    DEFINES AN ORDERED LIST OF THE POSSIBLE PROBABILITIES
    
    Its going to be used for determining the data order and their index
"""
probabilities = ['Neutral','Happy','Sad','Surprise','Fear','Disgust','Anger','Contempt']


def tableSession(name):
    
    """DEFINES AN ORDERED LIST OF THE POSSIBLE PROBABILITIES
    
    Its going to be used for determining the data order and their index
    """
    
    return f"""
        create table if not exists {name}(
            id bigint auto_increment primary key,
            time datetime default current_timestamp
        );

    """


def tableFrame(name,parent):
    
    """FUNCTION WHICH DEFINES AND RETURNS THE QUERY FOR THE FRAME TABLE WHICH THE PLUGIN USES
    
    This table is going to log all the data uploaded, when and by what parameters was it averaged
    """
    
    return f"""
        create table if not exists {name}(
            id bigint auto_increment primary key,
            session_id bigint,
            foreign key (session_id) references {parent}(id),
            face_count int,
            average_of_frames int,
            time datetime default current_timestamp
        );

    """



def tableFace(name, parent):
    
    """FUNCTION WHICH DEFINES AND RETURNS THE QUERY FOR THE FACE TABLE WHICH THE PLUGIN USES
    
    This table is going to store all the data originated from the face analytics.
    Each row represents one face and their data.
    These rows are connected to the logs in the FRAMES table.
    """
    
    return f"""
        create table if not exists {name}(
            id bigint auto_increment primary key,
            frame_id bigint,
            foreign key (frame_id) references {parent}(id),
            identity tinytext,
            predicted_emotion tinytext,
            predicted_emotion_index int,
            secondary_emotion tinytext,
            secondary_emotion_index int,
            neutral double,
            happy double,
            sad double,
            surprise double,
            fear double,
            disgust double,
            anger double,
            contempt double,
            valence double,
            arousal double
        );
            """
            

            
def defaultStruct(identity, predictOne, predictOneIn, predictSec, predictSecIn, neutral, happy, sad, surprise, fear, disgust, anger, contempt, valence, arousal):
    
    """GENERATE ONE FACE DATA
    Represents the data structure of one face.
    Takes all the arguments and returns a dictionary.
    """
    
    return {'Identity': identity,
            'PredictedEmotion': predictOne,
            'PredictedEmotionIndex': predictOneIn,
            'SecondaryEmotion': predictSec,
            'SecondaryEmotionIndex': predictSecIn,
            'Probabilities':
                {'Neutral': neutral,
                 'Happy': happy,
                 'Sad': sad,
                 'Surprise': surprise,
                 'Fear': fear,
                 'Disgust': disgust,
                 'Anger': anger,
                 'Contempt': contempt
                 },
            'Valence': valence,
            'Arousal': arousal
            }

class Person():
    
    """Defines a data class to help us work with the gotten data from the analyser.
    """
        
    def __init__(self, args:dict = {}):
        
        self.data = args

    def getDataList(self):
        """Returns the data in a list. Its used for filling in the values in case of an SQL query."""
        return [self.data['Identity'],
                self.data['PredictedEmotion'],
                self.data['PredictedEmotionIndex'],
                self.data['SecondaryEmotion'],
                self.data['SecondaryEmotionIndex'],
                self.data['Probabilities']['Neutral'],
                self.data['Probabilities']['Happy'],
                self.data['Probabilities']['Sad'],
                self.data['Probabilities']['Surprise'],
                self.data['Probabilities']['Fear'],
                self.data['Probabilities']['Disgust'],
                self.data['Probabilities']['Anger'],
                self.data['Probabilities']['Contempt'],
                self.data['Valence'],
                self.data['Arousal']
                ]
        
    def dataParse(self, args):
        """Parsing list of arguments with the correct keywords."""
        self.data['Identity'] = args[0]
        self.data['PredictedEmotion'] = args[1]
        self.data['PredictedEmotionIndex'] = args[2]
        self.data['SecondaryEmotion'] = args[3]
        self.data['SecondaryEmotionIndex'] = args[4]
        self.data['Neutral'] = args[5]
        self.data['Happy'] = args[6]
        self.data['Sad'] = args[7]
        self.data['Surprise'] = args[8]
        self.data['Fear'] = args[9]
        self.data['Disgust'] = args[10]
        self.data['Anger'] = args[11]
        self.data['Contempt'] = args[12]
        self.data['Valence'] = args[13]
        self.data['Arousal'] = args[14]
        return self.data
        
                
    def setEmotion(self, emotion, value):
        """Setting an emotion to the selected value in the data set."""
        self.data['Probabilities'][emotion] = value
        return value
    
    def setData(self, data, value):
        """Setting the data to the selected value in the data set."""
        self.data[data] = value
        return value
        
    def getEmotion(self, emotion):
        return self.data['Probabilities'][emotion]

    def getData(self, data):
        return self.data[data] 
        
    def addToEmotion(self, emotion, value):
        """Adding together two emotion values."""
        self.data['Probabilities'][emotion] = self.getEmotion(emotion) + value

    def addToData(self, data, value):
        """Adding together two data values."""
        self.data[data] = self.getData(data) + value
        
    def setPrediction(self):
        
        """Gerating a prediction based on the highest values. It sets the primary and secondary emotion."""
        
        data = self.getData('Probabilities').items()
        sortedList = sorted(data, key=lambda x: x[1], reverse=True)
        
        id = probabilities.index(sortedList[0][0])
        self.setData('PredictedEmotion', sortedList[0][0])
        self.setData('PredictedEmotionIndex', id)
        
        id = probabilities.index(sortedList[1][0])
        self.setData('SecondaryEmotion', sortedList[1][0])
        self.setData('SecondaryEmotionIndex', id)
        

class Extension(Plugin):
    
    """This plugin serves the purpose to save the data output from the face and emotion analyser to a database.
    To reduce the amount of data uploaded into the database we are averaging the data.
    
    :param login: File which contains the database login credentials.
    :param frameTable: Name of the table containing the information about the frames.
    :param faceTable: The name of the table cointaining the data for the faces.
    :param sessionTable: Name of the table logging the sessions.
    :param dataBuffer: The list holding the data to be averaged.
    :param bufferSize: Maximum amount of data held in the buffer.
    :param currentStage: Number of data in the buffer.
    :param mode: Mode of the averaging. True for Library mode, False for the largest mode.
    :param connection: Link to the database.
    :param dataLine: Second connection to the database for reading the data independently.
    
    """
    
    def average(self):
        
        """ 
        AVERAGING THE BUFFED ITEMS
        
        Takes all the frame information from the buffer.
        And creates an average output for each identity which is then returned.
        
        :return: Dictionary with the Persons identified by their Identity name
        """
        
        overall : dict(str, Person)= {}
        
        divideBy = {}
        
        for x in self.dataBuffer:
            
            identity = x['Identity']
            
            if identity not in overall:
                overall[identity] = Person(x)
                divideBy[identity] = 1
                continue
            
            for y in probabilities:
                overall[identity].addToEmotion(y, x['Probabilities'][y])
            
            overall[identity].addToData('Valence', x['Valence'])
            overall[identity].addToData('Arousal', x['Arousal'])
            
            divideBy[identity] = divideBy[identity] + 1

        for x in overall:
            data = overall[x]
            
            possId = 0
            currentMax = -1
            
            for y in probabilities:
                
                val = overall[x].setEmotion(y, overall[x].getEmotion(y) / divideBy[x] )
                if val > currentMax:
                    currentMax = val
                possId += 1
                
            overall[x].setData('Valence', data.getData('Valence') / divideBy[x])
            overall[x].setData('Arousal', data.getData('Arousal') / divideBy[x])

            overall[x].setPrediction()
        
        return overall
    
    def insert(self, data):
        """ 
        CREATING DATABASE LOG IN THE CONNECTED DATABASE
        
        Creates one row for the log in the logs table.
        Creates data rows for each face pointing to the log.
        
        :param data: The dictionary of averaged data by Person.
        """
        cursor = self.connection.execute(
            f"insert into {self.frameTable} (session_id,face_count,average_of_frames) values (%s, %s , %s)",
            True,
            (self.sessionId,
             len(data),
             len(self.dataBuffer)))
        nextId = int(cursor.lastrowid)
        for x in data:
            self.connection.execute(f"""insert into {self.faceTable} 
                                    (frame_id,
                                    identity,
                                    predicted_emotion,
                                    predicted_emotion_index,
                                    secondary_emotion,
                                    secondary_emotion_index,
                                    neutral,
                                    happy,
                                    sad,
                                    surprise,
                                    fear,
                                    disgust,
                                    anger,
                                    contempt,
                                    valence,
                                    arousal
                                    ) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                                    True,
                                    [nextId] + data[x].getDataList()
                                    )
            
    def modeLibrary(self,args):
        
        """MODE LIBRARY WORKS WITH THE IDENTIFIED FACES
        
        This function can work and average the faces if they are identified.
        All the unidentified faces going to be averaged under one category.
        The averaged data is then uploaded to the database.
        """
        
        for x in args['data']['Faces']: 
            self.dataBuffer.append( x )
            
        self.currentStage += 1
        
        if self.currentStage != self.bufferSize:
            return args
        
        self.currentStage = 0
        data = self.average()
    
        self.insert(data)
        self.dataBuffer.clear()

    
    def modeLargest(self, args:dict):
        
        """MODE LARGEST PROCESSES THE LARGEST FACE IN THE FRAME
        
        Picks out and adds the information about the largest face into the buffer.
        If the buffer achieved the required size it asks to average the buffer information.
        Then uploading the averaged data into the database.
        
        """

        largest = {}
        largestSize = 0
        
        for x in args['data']['Faces']:
            currentSize = x['Position']['width'] * x['Position']['height']
            if currentSize > largestSize:
                largestSize = currentSize
                largest = x
                
        self.dataBuffer.append( largest )
        self.currentStage += 1
        
        if self.currentStage != self.bufferSize:
            return args
        
        self.currentStage = 0
        data = self.average()

        self.insert(data)
        self.dataBuffer.clear()
    
    
    
    
    def process(self, args:dict):
        
        """PROCESSING THE DATA
        
        Skips if there is no usable information between the arguments
        Launches the selected mode
        - Library mode - processes faces based on the faces found in the library - can't average on unknown faces
        - Largest mode - processes only the largest face on the image - can be used for videocalls.
        """
        
        if not self.active or 'data' not in args:
            return args
        
        if self.mode:
            self.modeLibrary(args)
        else:
            self.modeLargest(args)
                    
        return args
    
    
    
    def init(self):
        
        """
        INITIALISE THE PLUGIN AND CREATE A DATABASE CONNECTION
        
        It creates his own tables if they dont exist yet.
        """
        
        self.name = "Database data logger for analysation data"
        self.login = "src\\plugins\\login.json"
        self.frameTable = "frame"
        self.faceTable = "face"
        self.sessionTable = "session"
        self.dataBuffer=[]
        self.bufferSize=5
        self.currentStage=0
        self.mode = False
 
        self.connection = DatabaseManager(self.login)
        self.connection.connectDatabase()
        
        cursor = self.connection.execute(tableSession(self.sessionTable),False)
        cursor = self.connection.execute(f"insert into {self.sessionTable} () values ()",True)
        self.sessionId = int(cursor.lastrowid)
        
        self.connection.execute(tableFrame(self.frameTable,self.sessionTable),False)
        self.connection.execute(tableFace(self.faceTable,self.frameTable),False)
        
    def edit(self) -> dict:
        return {'Login file':self.login,
                'Session table':self.sessionTable,
                'Frame table':self.frameTable,
                'Face table':self.faceTable,
                'Averaging size':self.bufferSize,
                'Averaging mode':self.mode,
                'Active':self.active
                }
    
    def update(self, input: dict):
        for i in self.edit():
            if i not in input:
                return -1
            
        if input['Active'] == "True":
            self.active=True
        elif input['Active'] == "False":
            self.active=False
        self.login = input['Login file']
        self.sessionTable = input['Session table']
        self.frameTable = input['Frame table']
        self.faceTable = input['Face table']
        self.bufferSize = input['Averaging size']
        
        if input['Averaging mode'] == "True":
            self.mode = True
        elif input['Averaging mode'] == "False":
            self.mode = False
        
        self.init()
        return 1
    
    def getData(self):
        try:
            cursor = self.connection.execute(f"SELECT id FROM {self.frameTable} WHERE session_id = {self.sessionId} ORDER BY ID DESC LIMIT 1;",False)
            id = cursor.fetchall()
            cursor = self.connection.execute(f"SELECT * FROM {self.faceTable} WHERE frame_id = {id[0][0]};",False)
            data = cursor.fetchall()
            
            allProcessed = {}
            
            for face in data:
                face = face[2:]
                person = Person()
                person.dataParse(face)
                allProcessed[person.data['Identity']] = person.data
            
            return allProcessed
        except:
            return -1
        
# ext = Extension()
# ext.init()

# ext.dataBuffer.append({'Identity': 'Benedek', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 1, 'Happy': 1, 'Sad': 1, 'Surprise': 1, 'Fear': 1, 'Disgust': 1, 'Anger': 1, 'Contempt': 1}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}})
# ext.dataBuffer.append({'Identity': 'Benedek', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 2, 'Happy': 2, 'Sad': 2, 'Surprise': 2, 'Fear': 2, 'Disgust': 2, 'Anger': 2, 'Contempt': 2}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}})
# ext.dataBuffer.append({'Identity': 'Benedek', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 3, 'Happy': 3, 'Sad': 3, 'Surprise': 5, 'Fear': 3, 'Disgust': 3, 'Anger': 3, 'Contempt': 3}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}})
# ext.dataBuffer.append({'Identity': 'Zsombor', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 2, 'Happy': 2, 'Sad': 2, 'Surprise': 2, 'Fear': 2, 'Disgust': 5, 'Anger': 2, 'Contempt': 2}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}})


# ext.modeLibrary({'data' : {'FaceCount': 1, 'Faces':[{'Identity': 'Benedek', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 1, 'Happy': 1, 'Sad': 1, 'Surprise': 1, 'Fear': 1, 'Disgust': 1, 'Anger': 1, 'Contempt': 1}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}}]}})
# ext.modeLibrary({'data' : {'FaceCount': 1, 'Faces':[{'Identity': 'Benedek', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 2, 'Happy': 2, 'Sad': 2, 'Surprise': 2, 'Fear': 2, 'Disgust': 2, 'Anger': 2, 'Contempt': 2}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}}]}})
# ext.modeLibrary({'data' : {'FaceCount': 1, 'Faces':[{'Identity': 'Benedek', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 3, 'Happy': 3, 'Sad': 3, 'Surprise': 5, 'Fear': 3, 'Disgust': 3, 'Anger': 3, 'Contempt': 3}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}}]}})
# ext.modeLibrary({'data' : {'FaceCount': 1, 'Faces':[{'Identity': 'Zsombor', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 2, 'Happy': 2, 'Sad': 2, 'Surprise': 2, 'Fear': 2, 'Disgust': 5, 'Anger': 2, 'Contempt': 2}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}}]}})
# ext.modeLibrary({'data' : {'FaceCount': 1, 'Faces':[{'Identity': 'Zsombor', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': {'Neutral': 2, 'Happy': 2, 'Sad': 2, 'Surprise': 2, 'Fear': 2, 'Disgust': 5, 'Anger': 2, 'Contempt': 2}, 'Valence': 1, 'Arousal': 1, 'Position': {'x': 301, 'y': 130, 'width': 104, 'height': 132}}]}})


# ext.modeLargest(
#     {'data' : {'FaceCount': 1, 'Faces': 
#         [
#             {'Identity': 'Zsombor', 'PredictedEmotion': 'Happy', 'PredictedEmotionIndex': 1, 'SecondaryEmotion': 'Contempt', 'SecondaryEmotionIndex': 7, 'Probabilities': 
#                 {'Neutral': 2, 'Happy': 2, 'Sad': 2, 'Surprise': 2, 'Fear': 2, 'Disgust': 2, 'Anger': 2, 'Contempt': 2
#                 }
#             , 'Valence': 1, 'Arousal': 1, 'Position': 
#                 {'x': 301, 'y': 130, 'width': 104, 'height': 132
#                  }
#             }
#         ]
#     }})