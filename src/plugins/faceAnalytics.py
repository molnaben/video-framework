import os.path

from plugins.pluginTemplate import Plugin
from plugins.VizEmo.FERecognizer import FERecognizer
from plugins.VizEmo.FaceIdentifier import FaceIdentifier
import os

class Extension(Plugin):
    
    """This plugin uses the VizEmo package to identify the faces and to detect the emotions.
    The data is then saved to the arguments while the frame is updated.
    
    :param faceDetection: Boolean to determine if we have face detection on. True if face detection is active.
    :param face_id: If faceDetection is active this contains an instance of the face identifier.
    :param fer: This is the instance of the analyser.
    """
    
    def process(self, args:dict):
        """If there is a frame in the arguments we send it for analysis and the returned data is saved to the arguments."""
        
        if not self.active:
            return args
        
        if 'frame' not in args :
            return args
            
        res = self.fer.process(args['frame'])
        args['data'] = res
        args['frame'] = self.fer.visualize(args['frame'], res)
            
        return args
    
    def init(self):
        self.name = "Frame analyser"
        self.faceDetection = False
        
        self.face_id = None
        self.fer = FERecognizer(print_info=False, face_model="centerface", identifier=self.face_id)
        
    def setFaceDetection(self, value: bool):
        
        """Setting up the face identifier."""
        
        if value:
            self.face_id = FaceIdentifier(os.path.dirname(os.path.realpath(__file__)) + "\\VizEmo\\images\\ids")
            self.fer=FERecognizer(print_info=False, face_model="centerface", identifier=self.face_id)
        else:
            self.face_id = None
            self.fer=FERecognizer(print_info=False, face_model="centerface", identifier=self.face_id)
            
    def edit(self) -> dict:
        return {'Face identification':self.faceDetection,'Active':self.active}
    
    def update(self, input: dict):
        
        if 'Face identification' not in input:
            return -1
        
        if input['Face identification'] == self.faceDetection:
            return 1
        
        value = False
        if input['Face identification'] == "True":
            value = True
            
        elif input['Face identification'] == "False":
            value = False
        else:
            return -1
         
        self.setFaceDetection(value)
        
        if input['Active'] == "True":
            self.active=True
        elif input['Active'] == "False":
            self.active=False
        return 1