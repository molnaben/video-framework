from flask import make_response
from plugins.pluginTemplate import Plugin
import cv2


class Extension(Plugin):
    
    """This plugins gives us the option to see a snapshot from the processing pipeline depending on the position of the plugin.
    
    :param frame: The last frame captured.
    """
    
    def process(self, args:dict):
        """Every time its called is saves the current state of the frame."""
        if 'frame' in args and self.active:
            ret, jpeg = cv2.imencode('.jpg', args['frame'])
            self.frame=jpeg
        return args
    
    def init(self):
        self.name = "Get live frame"
        self.frame=[]
    
    def getData(self):
        
        """Returns the last frame in an image response."""
        
        if self.frame == []:
            return -1
        response = make_response(self.frame.tobytes())
        response.headers['Content-Type'] = 'image/png'
        return response