from plugins.pluginTemplate import Plugin
import cv2


class Extension(Plugin):
    
    """This plugin translates the current frame to grayscale."""
    
    def process(self, args:dict):
        """Translating the current frame in the arguments to grayscale if there is one in the arguments."""
        if 'frame' in args and self.active :
            grayImage = cv2.cvtColor(args['frame'], cv2.COLOR_BGR2GRAY)
            args['frame']=grayImage
        
        return args
    
    def init(self):
        self.name = "Translate frame to grayscale"
        