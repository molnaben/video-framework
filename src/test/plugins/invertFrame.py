from plugins.pluginTemplate import Plugin
import cv2


class Extension(Plugin):
    
    """This plugin inverts the current frame."""
    
    def process(self, args:dict):
        """If there is a frame in the arguments it will be translated to its inverse."""
        if 'frame' in args and self.active:
            args['frame']=cv2.bitwise_not(args['frame'])
        return args
    
    def init(self):
        self.name = "Frame color inverter"
        