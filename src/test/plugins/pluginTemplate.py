from abc import ABC, abstractmethod


class Plugin(ABC):
    def __init__(self):
        self.active = True
        self.name = ""
        self.init()
    
    @abstractmethod
    def process(self, args) -> dict:
        pass
    
    @abstractmethod
    def init(self):
        pass
    
    def edit(self) -> dict:
        return {'Active':self.active}
    
    def update(self, input: dict):
        if input['Active'] == "True":
            self.active=True
        elif input['Active'] == "False":
            self.active=False
    
    def setActive(self):
        self.active = True
        return 0
    
    def setOffline(self):
        self.active = False
        return 0
    
    def getState(self):
        return self.active
    
    def getName(self):
        return self.name
    
    def getData(self):
        return -1