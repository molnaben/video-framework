import sys

sys.path.insert(1, '..\\')
from buffer import Buffer

from camera import Camera


camera = Camera(0,"video","vid1.mp4")
buffer = Buffer(camera)

def test_setMode():
    buffer.setMode("infinity")
    assert buffer.mode == "infinity"

    buffer.setMode("throw-old")
    assert buffer.mode == "throw-old"
    
    buffer.setMode("throw-new")
    assert buffer.mode == "throw-new"
    
    ret = buffer.setMode("notworking")
    assert ret == -1
    
def test_readConf():
    
    ret = buffer.readConfig("..\\buffer_default.json")
    assert ret == 1
    
    ret = buffer.readConfig("notworking")
    assert ret == -1
    
def test_request_add_one():
    
    camera.start()
    buffer.requestFrame()
    camera.stop()
    assert buffer.queue.qsize() == 1
    
def test_request_add_two():
    
    camera.start()
    buffer.requestFrame()
    camera.stop()
    assert buffer.queue.qsize() == 2
    
def test_request_pop():
    
    camera.start()
    buffer.popFrame()
    assert buffer.amount == 1
    camera.stop()
    
def test_clear():
    
    camera.start()
    
    buffer.requestFrame()
    buffer.requestFrame()
    
    buffer.clear()
    camera.stop()
    
    assert buffer.queue.qsize() == 0
    

