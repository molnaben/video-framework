import sys
sys.path.insert(1, '..\\')

from bufferManager import BufferManager
from cameraManager import CameraManager

camManager = CameraManager()
bufferManager = BufferManager()
cam = camManager.addNew("vid1.mp4","name")

def test_listActive_empty():
    list = bufferManager.listActiveCameras()
    assert len(list) == 0
    
def test_setCamLive_quitLive():
    ret = bufferManager.setCameraLive(cam)
    ret2 = bufferManager.quitLive(cam)
    assert ret == 1 and ret2 == 1
    
def test_setCamLive_alreadyLive():
    bufferManager.setCameraLive(cam)
    ret = bufferManager.setCameraLive(cam)
    bufferManager.quitLive(cam)
    assert ret == -1
    
def test_setCamLive_alreadyLive():
    bufferManager.setCameraLive(cam)
    ret = bufferManager.setCameraLive(cam)
    bufferManager.quitLive(cam)
    assert ret == -1
    
def test_quitLive_notLive():
    ret = bufferManager.quitLive(cam)
    assert ret == -1
    
def test_listActive():
    bufferManager.setCameraLive(cam)
    ret = bufferManager.listActiveCameras()
    length = len(ret)
    bufferManager.quitLive(cam)
    assert length == 1
    
def test_getFrame_wrondID():
    ret,frame = bufferManager.getFrameFromId(1)
    assert ret == -1
    
def test_getFrame():
    bufferManager.setCameraLive(cam)
    ret,frame = bufferManager.getFrameFromId(0)
    bufferManager.quitLive(cam)
    assert ret == 1