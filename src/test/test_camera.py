import pytest
import sys
sys.path.insert(1, '..\\')
from camera import Camera


def test_startStop():
    camera = Camera(0,"webcam",0)
    
    camera.start()
    on = camera.getStatus()
    camera.stop()
    off = camera.getStatus()
    assert on==True and off==False
    
def test_frame():
    camera = Camera(0,"webcam",0)
    
    camera.start()
    ret,frame = camera.getFrame()
    
    camera.stop()
    
    assert ret
    
def test_config():
    camera = Camera(0,"webcam",0)
    
    """Turned off camera"""
    off = camera.setConfig(camera.getConfigPath())
    
    """Set OK"""
    ok = camera.start()
    
    """Wrong path"""
    nonexist = "sajt"
    wrong = camera.setConfig(nonexist)
    
    camera.stop()
    assert off == -1 and ok == 1 and wrong == -1

def test_stop_wrong():
    camera = Camera(0,"webcam",0)
    
    """Camera isnt online"""
    assert camera.stop() == -1
    
def test_stop():
    camera = Camera(0,"webcam",0)
    
    """Camera is online"""
    camera.start()
    assert camera.stop() == 1
    