import sys

from cameraManager import CameraManager
sys.path.insert(1, '..\\')


def test_add():
    manager = CameraManager()
    manager.addNew(input=0,name="one")
    manager.addNew(input=0,name="two")
    
    assert manager.getNameOf(0) == "one" and  manager.getNameOf(1) == "two"
    
def test_delete():
    manager = CameraManager()
    manager.addNew("0","one")
    manager.addNew("0","two")
    
    assert manager.delete(0) == 1 and manager.delete(0) == -1
    
def test_update():
    manager = CameraManager()
    manager.addNew("0","one")
    
    manager.update(0,"new","source")
    
    assert manager.getNameOf(0) == "new" and manager.getSourceOf(0) == "source"
    
def test_update_wrong():
    manager = CameraManager()
    manager.addNew("0","one")
    
    ret = manager.update(100,"not","working")
    assert ret == -1
    
def test_setConfig_wrong():
    manager = CameraManager()
    manager.addNew("0","one")
    
    ret = manager.setConfig(100,"wrong id")
    assert ret == -1
    
def test_setConfig_working():
    manager = CameraManager()
    manager.addNew("0","one")
    
    manager.setConfig(0, "newpath")
    assert manager.getConfigOf(0) == "newpath"
    
def test_getDictData_empty():
    
    manager = CameraManager()
    
    for i in manager.getDictData():
        manager.delete(i)
    
    length = len(manager.getDictData())
    print(manager.getDictData())
    
    assert length == 0

def test_getDictData():
    manager = CameraManager()
    
    manager.addNew("0","one")
    manager.addNew("0","two")
    
    length = len(manager.getDictData())
    
    assert length == 2
