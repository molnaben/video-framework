import sys
sys.path.insert(1, '..\\')
from jsonEditor import write,read


def test_wrongpath_read():
    ret = read("nonexisting.json")
    assert ret == -1
    
def test_goodpath_read():
    ret = read("test.json")
    assert ret == {"data":"correct"}
    
def test_write():
    ret = write("test.json",{"data":"correct"})
    assert ret == 1 and read("test.json") == {"data":"correct"}
    