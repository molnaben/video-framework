import sys
from time import sleep
sys.path.insert(1, '..\\')

from outputManager import OutputManager
from cameraManager import CameraManager
from bufferManager import BufferManager
from pluginCollector import PluginCollector

cameraManager = CameraManager()
cameraManager.addNew("vid1.mp4","video")

bufferManager = BufferManager()

pluginCollector = PluginCollector()
pluginCollector.addManager("manager")

outputManager = OutputManager(cameraManager,bufferManager,pluginCollector)

def test_live():
    ret = outputManager.start(1,0,{},"manager")
    active = outputManager.getActiveOutputs()
    sleep(1)
    outputManager.quit(0)
    assert ret == 1 and active[0] == ["Live view","manager"]
    
def test_wrong_cam():
    ret = outputManager.start(1,5,{},"manager")
    assert ret == -4
    
def test_wrong_manager():
    ret = outputManager.start(1,0,{},"manager1")
    assert ret == -3

def test_wrong_mode():
    ret = outputManager.start(4,0,{},"manager")
    assert ret == -1
    
def test_file_notspec():
    ret = outputManager.start(2,0,{},"manager")
    assert ret == -5
    
def test_fps_notspec():
    ret = outputManager.start(2,0,{"filename":"test.avi"},"manager")
    assert ret == -5
    
def test_filesave():
    ret = outputManager.start(2,0,{"filename":"dump_test.avi","fps":24},"manager")
    active = outputManager.getActiveOutputs()
    sleep(1)
    outputManager.quit(0)
    assert ret == 1 and active[0] == ["Save to file","manager"]
    
def test_process():
    ret = outputManager.start(3,0,{},"manager")
    active = outputManager.getActiveOutputs()
    sleep(1)
    outputManager.quit(0)
    assert ret == 1 and active[0] == ["Only process","manager"]