import sys
sys.path.insert(1, '..\\')

from pluginManager import PluginManager


pluginManager = PluginManager()

def test_register_wrong():
    ret = pluginManager.registerPlugin("wrong.py")
    assert ret == -1
    
def test_register():
    ret = pluginManager.registerPlugin(".blackNwhite")
    assert ret == 1 
    
def test_register_two():
    ret = pluginManager.registerPlugin(".invertFrame")
    assert ret == 1 and len(pluginManager.getList()) == 2

def test_move_up():
    plugins = pluginManager.getList()
    ret = pluginManager.moveUp(1)
    pluginsModified = pluginManager.getList()
    assert ret == 1 and plugins[1] == pluginsModified[0]
    
def test_move_down():
    plugins = pluginManager.getList()
    ret = pluginManager.moveDown(0)
    pluginsModified = pluginManager.getList()
    assert ret == 1 and plugins[0] == pluginsModified[1]
    
def test_edit():
    ret = pluginManager.editPlugin(0)
    assert ret == {'Active':True}
    
def test_update():
    pluginManager.updatePlugin(0,{'Active':False})
    ret = pluginManager.editPlugin(0)
    assert ret['Active'] == True